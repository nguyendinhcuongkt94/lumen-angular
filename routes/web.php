<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('login' , 'backend\LoginController@login');
    $router->get('test' , 'backend\UserController@gets');
    $router->post('changepassword/{id}' , 'backend\LoginController@changePassword');
    $router->get('categories', 'backend\CategoryController@index');
    $router->get('categories/{id}', 'backend\CategoryController@detail');
    $router->put('categories/{id}', 'backend\CategoryController@update');
    $router->post('categories/add', 'backend\CategoryController@store');
    $router->delete('categories/{id}', 'backend\CategoryController@remove');

    $router->get('categories/news/{id}', 'backend\NewsController@refineNewsWithCategoryId');
    $router->get('news', 'backend\NewsController@index');
    $router->get('news/detail/{id}', 'backend\NewsController@detail');
    $router->put('news/edit/{id}', 'backend\NewsController@update');
    $router->post('news/add', 'backend\NewsController@store');
    $router->post('news/search', 'backend\NewsController@search');
    $router->delete('news/delete/{id}', 'backend\NewsController@remove');

    $router->get('users', 'backend\UserController@index');
    $router->get('users/{id}', 'backend\UserController@detail');
});
