<?php

use Illuminate\Database\Seeder;

class category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert([
            ['name' => 'World'],
            ['name' => 'Sports'],
            ['name' =>'Business'],
            ['name' =>'Science'],
            ['name'=>'Health'],
            ['name'=>'Books'],
            ['name'=>'Travel']
        ]);
    }
}
