<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class user extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 10;
        for ($i = 0; $i < $limit; $i++) {
            DB::table('user')->insert([
                'email'=>$faker->email,
                'password'=>Hash::make('123456'),
                'full'=>$faker->name,
                'address'=>$faker->streetAddress,
                'phone'=>$faker->e164PhoneNumber,
                'level'=>rand(0,1)
            ]);
        }
    }
}
