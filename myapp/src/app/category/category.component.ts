import { Component, OnInit } from "@angular/core";
import { CategoryService } from "src/app/category.service";
import { Category } from "src/app/models/category";
import { ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
@Component({
  selector: "app-category",
  templateUrl: "./category.component.html",
  styleUrls: ["./category.component.css"]
})
export class CategoryComponent implements OnInit {
  categories: Category[];
  category: Category = { id: 1, name: "" };
  @ViewChild("closebutton", { static: true }) closebutton;
  @ViewChild("closemodal", { static: true }) closemodal;
  constructor(
    private categoryService: CategoryService,
    private route: ActivatedRoute
  ) {}

  gets(): void {
    this.categoryService.gets().subscribe(result => {
      this.categories = result;
    });
  }

  get(id: number): void {
    this.categoryService.get(id).subscribe(result => {
      this.category = result;
    });
  }

  addCategory(name: string): void {
    const newCategory: Category = new Category();
    newCategory.name = name;
    this.categoryService.add(newCategory).subscribe(result => {
      this.categories.push(result);
    });
    this.closebutton.nativeElement.click();
  }

  edit(id: number): void {
    this.categoryService.edit(this.category).subscribe();
    let category = this.categories.find(category => category.id == id);
    category.name = this.category.name;
    this.closemodal.nativeElement.click();
  }

  remove(id: number): void {
    const message = confirm("Do you want to remove this category ?");
    if (message == true) {
      this.categoryService.delete(id).subscribe(() => {
        this.categories = this.categories.filter(
          eachCategory => eachCategory.id !== id
        );
      });
    }
  }
  ngOnInit() {
    this.gets();
  }
}
