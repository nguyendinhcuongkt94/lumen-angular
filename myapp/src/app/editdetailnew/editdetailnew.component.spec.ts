import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditdetailnewComponent } from './editdetailnew.component';

describe('EditdetailnewComponent', () => {
  let component: EditdetailnewComponent;
  let fixture: ComponentFixture<EditdetailnewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditdetailnewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditdetailnewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
