import { Component, Input, OnInit } from "@angular/core";
import { News } from "src/app/models/news";
import { NewsService } from "src/app/news.service";
import { ActivatedRoute } from "@angular/router";
import * as ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { Location } from "@angular/common";
import { CategoryService } from "src/app/category.service";
import { Category } from "src/app/models/category";
import { Form } from "src/app/models/formdata";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import {
  AngularFireStorage,
  AngularFireUploadTask
} from "@angular/fire/storage";
@Component({
  selector: "app-editdetailnew",
  templateUrl: "./editdetailnew.component.html",
  styleUrls: ["./editdetailnew.component.css"]
})
export class EditdetailnewComponent implements OnInit {
  public Editor = ClassicEditor;
  new: News;
  selectedFile: File;
  category: Category[];
  editnew: FormGroup;
  task: AngularFireUploadTask;
  src:string;
  constructor(
    private route: ActivatedRoute,
    private news: NewsService,
    private location: Location,
    private categories: CategoryService,
    private fb: FormBuilder,
    private storage: AngularFireStorage
  ) {}

  updateNew(): void {
    if (this.selectedFile) {
      const urlFile = `imgupload/${this.selectedFile.name}`;
      this.task = this.storage.upload(urlFile, this.selectedFile);
    }
    this.news.editNew(this.new).subscribe(() => this.goBack());
  }

  detailNew(): void {
    const id = +this.route.snapshot.paramMap.get("id");
    this.news.getNew(id).subscribe(result => {
      this.new = result;
    });
  }

  getCategories(): void {
    this.categories.gets().subscribe(result => {
      this.category = result;
    });
  }

  onSelectImg(event): void {
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();
      reader.onload = (event: any) => {
        this.src = event.target.result;
      };
      reader.readAsDataURL(event.target.files[0]);
      this.new.news.img = event.target.files[0].name;
      this.selectedFile = event.target.files[0];
    }
  }

  goBack(): void {
    this.location.back();
  }

  ngOnInit() {
    this.detailNew();
    this.getCategories();
  }
}
