import { Injectable } from '@angular/core';
import { Category } from 'src/app/models/category';
import { Observable,of } from 'rxjs';
import { HttpClient,HttpHeaders, HttpClientModule} from '@angular/common/http';
import { AsyncValidatorFn,AbstractControl,ValidationErrors } from '@angular/forms';

const httpOptions = {
  headers:new HttpHeaders({'Content-Type':'application/json'})
};
@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private categoryUrl = "http://localhost:8000/api/categories";
  categories:Category[];

  gets():Observable<Category[]>{
    return this.http.get<Category[]>(this.categoryUrl)
  }

  add(category:Category):Observable<Category>{
    return this.http.post<Category>(`${this.categoryUrl}/add`,category,httpOptions)
  }

  edit(category:Category):Observable<any>{
    return this.http.put<Category>(`${this.categoryUrl}/${category.id}`,category,httpOptions);
  }

  get(id:number):Observable<Category>{
    return this.http.get<Category>(`${this.categoryUrl}/${id}`);
  }

  delete(id:number):Observable<Category>{
    const url = `${this.categoryUrl}/${id}`;
    return this.http.delete<Category>(url);
  }

  checkIfCategoryExists(cate): Observable<boolean> {
    return of(this.categories.includes(cate)).pipe();
  }

  // categoryValidator(): AsyncValidatorFn {
  //   return (control: AbstractControl): Observable<ValidationErrors | null> => {
  //     return this.checkIfCategoryExists(control.value).pipe(
  //       map(res => {
  //         return res ? { usernameExists: true } : null;
  //       })
  //     );
  //   };
  // }
  constructor(
    private http:HttpClient,
  ) { }
}
