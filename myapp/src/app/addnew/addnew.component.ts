import { Component, OnInit } from "@angular/core";
import { News } from "src/app/models/news";
import { NewsService } from "src/app/news.service";
import { Location } from "@angular/common";
import * as ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { CategoryService } from "src/app/category.service";
import { Category } from "src/app/models/category";
import { Form } from "src/app/models/formdata";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import {
  AngularFireStorage,
  AngularFireUploadTask
} from "@angular/fire/storage";
import {
  AngularFirestore,
  AngularFirestoreCollection
} from "@angular/fire/firestore";
import { Observable } from "rxjs";
import { finalize, tap } from "rxjs/operators";
export interface response {
  name: string;
  filepath: string;
  size: number;
}
@Component({
  selector: "app-addnew",
  templateUrl: "./addnew.component.html",
  styleUrls: ["./addnew.component.css"]
})
export class AddnewComponent implements OnInit {
  public Editor = ClassicEditor;
  task: AngularFireUploadTask;
  snapshot: Observable<any>;
  profileForm: FormGroup;
  new: News;
  selectedFile: File;
  category: Category[];
  formData: FormGroup;
  form = new Form("", "", "", 2, 1);
  images: Observable<response[]>;
  UploadedFileURL: Observable<string>;
  fileSize: number;
  src:string;
  private imageCollection: AngularFirestoreCollection<response>;

  constructor(
    private newsService: NewsService,
    private location: Location,
    private categories: CategoryService,
    // private http: HttpClient,
    private fb: FormBuilder,
    private storage: AngularFireStorage,
    private database: AngularFirestore
  ) {
    this.imageCollection = database.collection<response>("imgupload");
    this.images = this.imageCollection.valueChanges();
  }

  addNew() {
    let info = JSON.parse(localStorage.getItem("currentUser"));
    const urlFile = `imgupload/${this.selectedFile.name}`;
    //crete new
    this.form.user_id = info.id;
    this.newsService.addNew(this.form).subscribe(_ => this.goBack());

    //upload imgae to firebase
    this.task = this.storage.upload(urlFile, this.selectedFile);
    // this.snapshot = this.task.snapshotChanges().pipe(
    //   finalize(() => {
    //     // Get uploaded file storage path
    //     this.UploadedFileURL.subscribe(resp=>{
    //       this.addImagetoDB({
    //         name: this.selectedFile.name,
    //         filepath: resp,
    //         size: this.fileSize
    //       });
    //     },error=>{
    //       console.error(error);
    //     })
    //   }),
    //   tap(snap => {
    //       console.log(snap);
    //   })
    // );
  }

  getCategories(): void {
    this.categories.gets().subscribe(result => {
      this.category = result;
    });
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (event: any) => {
        this.src = event.target.result;
      };
      reader.readAsDataURL(event.target.files[0]);
      this.form.img = event.target.files[0].name;
      this.selectedFile = event.target.files[0];
    }
  }

  goBack(): void {
    this.location.back();
  }

  addImagetoDB(image: response) {
    //Create an ID for document
    const id = this.database.createId();

    //Set document id with value in database
    this.imageCollection
      .doc(id)
      .set(image)
      .then(resp => {
        console.log(resp);
      })
      .catch(error => {
        console.log("error " + error);
      });
  }

  ngOnInit() {
    this.getCategories();
    this.formData = this.fb.group({
      title: ["", [Validators.required]],
      content: ["", Validators.required],
      img: [""],
      category_id: ["2"],
      user_id: [""]
    });
  }
}
