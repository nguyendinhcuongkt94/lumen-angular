import { Component } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  constructor(
    private authService: AuthService
    ) { }
  title = 'myapp';
  checkLogin:boolean = false;

  check(){
    if(this.authService.isLoggedIn()==true){
      this.checkLogin = true;
    }
    else
      this.checkLogin = false;
  }

  logout(){
    this.authService.logout();
    window.location.href = '/login';
  }

  ngOnInit() {
    this.check(); 
  }
}
