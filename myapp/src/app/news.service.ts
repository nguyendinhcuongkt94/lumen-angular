import { Injectable } from '@angular/core';
import { News } from 'src/app/models/news';
import { Observable,of } from 'rxjs';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { Form } from 'src/app/models/formdata';
import { BehaviorSubject } from 'rxjs';

const httpOptions = {
  headers:new HttpHeaders({'Content-Type':'application/json'})
};
@Injectable({
  providedIn: 'root'
})
export class NewsService {
  private newsURL = "http://localhost:8000/api/news";
  private categoryUrl = "http://localhost:8000/api/categories";
  
  private messageSource = new BehaviorSubject('');
  currentMessage = this.messageSource.asObservable();

  changeMessage(message) {
    this.messageSource.next(message)
  }

  getNews(): Observable<News[]> {
    return this.http.get<News[]>(this.newsURL);
  };

  getNew(id:number): Observable<News> {
    const url = `${this.newsURL}/detail/${id}`;
    return this.http.get<News>(url)
  };

  editNew(data:News):Observable<any>{
    return this.http.put(`${this.newsURL}/edit/${data.news.id}`,data,httpOptions)
  };

  addNew(data)
  {
    return this.http.post<any>(`${this.newsURL}/add`,data);
  }

  deleteNew(id:number):Observable<News>
  {
    const url =`${this.newsURL}/delete/${id}`;
    return this.http.delete<News>(url,httpOptions);
  }

  refineNewsWithCategoryId(id):Observable<News[]>{
    const url = `${this.categoryUrl}/news/${id}`;
    return this.http.get<News[]>(url)
  }

  searchNews(data):Observable<any>{
    const url = `${this.newsURL}/search`;
    return this.http.post<any>(url,data);
  }

  constructor(
    private http:HttpClient
    ) { }
}
