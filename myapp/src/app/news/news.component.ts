import { Component, OnInit } from '@angular/core';
import { NewsService } from 'src/app/news.service';
import { News } from 'src/app/models/news';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  news:News[];
  message={'key':''};
  finished = false;
  originalPosts:News[];
  constructor(
    private newsService: NewsService,
    private location:Location,
    private route:ActivatedRoute,
    ) { }

  getNews():void{
    const id = +this.route.snapshot.paramMap.get('id');
    if(id==0){
      this.newsService.getNews().subscribe(
        (news)=>{
          this.originalPosts = news;
          this.news=news.slice(0,3);
        }
      )
    }
    else{
      this.newsService.refineNewsWithCategoryId(id).subscribe(
        (news)=>{
          this.originalPosts = news;
          this.news=news.slice(0,3);
        }
      )
    }
  }

  onScroll () {
    if(this.news){
      if(this.news.length < this.originalPosts.length){  
        let totalNewsShow = this.news.length;
        for(let i = totalNewsShow; i <= totalNewsShow+1; i++){
          this.news.push(this.originalPosts[i]);
        }
      }
      if(this.news.length == this.originalPosts.length){
        this.finished = true;
      }
    }
  }

  delete(id:number):void{
    const message = confirm('Do you want to delete this new !');
    if(message == true)
    {
      this.newsService.deleteNew(id).subscribe(
        ()=>{
          this.news = this.news.filter( eachNew => eachNew.news.id !== id)
        }
      )
    }
  }

  goBack():void{
    this.location.back();
  }

  ngOnInit() {
    this.getNews()
    this.newsService.currentMessage.subscribe(
      (message) => {
        this.message.key = message;
        if(this.message.key.length>=1){
        this.newsService.searchNews(this.message).subscribe(
          (news)=>{
            this.originalPosts = news;
            this.news=news.slice(0,3);
            this.onScroll();
          }
        )}
      })
    
  }

}
