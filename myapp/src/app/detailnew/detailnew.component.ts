import { Component, OnInit, Input } from '@angular/core';
import { News } from 'src/app/models/news';
import { NewsService} from 'src/app/news.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ThrowStmt } from '@angular/compiler';

@Component({

  selector: 'app-detailnew',
  templateUrl: './detailnew.component.html',
  styleUrls: ['./detailnew.component.css']
})
export class DetailnewComponent implements OnInit {
  new:News;
  constructor(
    private route:ActivatedRoute,
    private news:NewsService,
    private location:Location,
  ) { }
  
  getNew():void{
    const id=+this.route.snapshot.paramMap.get('id');
    this.news.getNew(id).subscribe(
      (result)=>{
        this.new=result
      }
    );
  }
  
  edit():void{
  }
  
  goBack(){
    this.location.back();
  }

  ngOnInit() {
    this.getNew()
    
  }

}
