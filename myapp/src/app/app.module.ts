import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { TopComponent } from './top/top.component';
import { ContentComponent } from './content/content.component';
import { CategoryComponent } from './category/category.component';
import { UserComponent } from './user/user.component';
import { NewsComponent } from './news/news.component';
import { NewsService } from 'src/app/news.service'
import { HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { DetailnewComponent } from './detailnew/detailnew.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { EditdetailnewComponent } from './editdetailnew/editdetailnew.component';
import { AddnewComponent } from './addnew/addnew.component';
import { environment } from 'src/environments/environment';
import { httpInterceptorProviders } from './http-interceptors/index';
import { LoginComponent } from './login/login.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    TopComponent,
    ContentComponent,
    CategoryComponent,
    UserComponent,
    NewsComponent,
    DetailnewComponent,
    EditdetailnewComponent,
    AddnewComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    CKEditorModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase, 'images'),
    AngularFirestoreModule, 
    AngularFireStorageModule,
    InfiniteScrollModule
  ],
  providers: [NewsService,httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
