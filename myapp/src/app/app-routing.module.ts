import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { NewsComponent } from "src/app/news/news.component";
import { CategoryComponent } from "src/app/category/category.component";
import { UserComponent } from "src/app/user/user.component";
import { DetailnewComponent } from "src/app/detailnew/detailnew.component";
import { EditdetailnewComponent } from "src/app/editdetailnew/editdetailnew.component";
import { AddnewComponent } from "src/app/addnew/addnew.component";
import { AuthGuard } from "src/app/auth.guard";
import { LoginComponent } from "src/app/login/login.component";
import { LogoutGuard } from "src/app/logout.guard";
const routes: Routes = [
  { path: "", redirectTo: "login", pathMatch: "full" },
  { path: "login", component: LoginComponent, canActivate: [LogoutGuard] },
  {
    path: "admin",
    canActivate: [AuthGuard],
    children: [
      { path: "", redirectTo: "categories", pathMatch: "full" },
      { path: "news", component: NewsComponent },
      { path: "categories", component: CategoryComponent },
      { path: "categories/news/:id", component: NewsComponent },
      { path: "news/search", component: NewsComponent },
      { path: "users", component: UserComponent },
      { path: "news/detail/:id", component: DetailnewComponent },
      { path: "news/edit/:id", component: EditdetailnewComponent },
      { path: "news/add", component: AddnewComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
