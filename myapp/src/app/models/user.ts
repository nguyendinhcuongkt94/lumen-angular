export class User
{
    "user": {
      "id": number;
      "email": string;
      "full": string;
      "address": string;
      "phone": number;
      "level": number;
    }
}