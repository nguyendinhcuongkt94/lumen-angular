export class News {
    'news':{
        'id':number;
        'title':string;
        'img':string;
        'content':string;
        'category_id':number;
        "user_id": number;
        'created_at':Date;
    };
    "category":string;
    "author": string;
}
