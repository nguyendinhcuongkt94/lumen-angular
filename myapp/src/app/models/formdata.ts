export class Form
{
    constructor(
        public title:string,
        public content:string,
        public img:string,
        public category_id:number,
        public user_id: number,
    ) {}
}