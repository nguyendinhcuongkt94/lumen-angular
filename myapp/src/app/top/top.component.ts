import { Component, OnInit } from "@angular/core";
import { NewsService } from "src/app/news.service";
import { AuthService } from "src/app/auth.service";
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validator,
  Validators
} from "@angular/forms";
import { ViewChild } from "@angular/core";

@Component({
  selector: "app-top",
  templateUrl: "./top.component.html",
  styleUrls: ["./top.component.css"]
})
export class TopComponent implements OnInit {
  @ViewChild("closebutton", { static: true }) closebutton;
  message: string;
  error: string;
  changePassword = this.fb.group({
    password: ["", [Validators.required, Validators.minLength(6)]],
    newpassword: ["", [Validators.required, Validators.minLength(6)]],
    renewpassword: ["", [Validators.required, Validators.minLength(6)]]
  });
  constructor(
    private newsService: NewsService,
    private authService: AuthService,
    private fb: FormBuilder
  ) {}

  newMesages(event) {
      this.message = event;
      this.newsService.changeMessage(this.message);
  }

  ngOnInit() {
    this.newsService.currentMessage.subscribe(
      message => (this.message = message)
    );
  }

  submit() {
    if(this.changePassword.value.password===this.changePassword.value.renewpassword){
      let data = this.changePassword.value;
      this.authService
        .changePassword(data)
        .subscribe((data) =>{ 
          if(data.success){
            this.closebutton.nativeElement.click();
            this.changePassword.reset();
            alert(data.success);
          }
          else{
            this.error = data.error;
          }
        });
    }
    else{
        this.error = "Password and confirm password does not match !"
    }
  }
}
