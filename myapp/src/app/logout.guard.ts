import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/auth.service';

@Injectable({
  providedIn: 'root'
})
export class LogoutGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    const url: string = state.url;
    return this.checkLogout(url);
  }

  checkLogout(url:string){
    if(this.authService.isLoggedIn()){
      this.authService.redirectUrl = url;
      this.router.navigate(['/admin/categories'], {queryParams: { returnUrl: url }} );
      return false
    }
    else{
      return true
    }
  }
}
