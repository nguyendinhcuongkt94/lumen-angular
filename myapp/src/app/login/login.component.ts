import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "../auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = true;
  returnUrl: string;
  loading = false;
  error: {};
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  onSubmit(loginForm) {
    this.submitted = false;
    this.loading = true;
    this.authService.login(loginForm.value).subscribe(data => {
      // localStorage.setItem("currentUser", JSON.stringify(data));
      // let user = JSON.parse(localStorage.getItem("currentUser"));
      if (this.authService.isLoggedIn()) {
        // const redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/admin';
        // this.router.navigate([redirect]);
        window.location.href = "/admin";
      } else {
        this.error = "Email or password is not correct !";
        this.loading = false;
        this.submitted = true;
      }
    });
  }
}
