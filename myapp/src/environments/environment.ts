// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyDrTrvBkAyT1QFgnwQgDpjJCA1HZ7dwY_M",
    authDomain: "api-firebase-b7ff2.firebaseapp.com",
    databaseURL: "https://api-firebase-b7ff2.firebaseio.com",
    projectId: "api-firebase-b7ff2",
    storageBucket: "api-firebase-b7ff2.appspot.com",
    messagingSenderId: "279021337215",
    appId: "1:279021337215:web:89af13874e579eab90dd4a",
    measurementId: "G-XZTBL88EY0"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
