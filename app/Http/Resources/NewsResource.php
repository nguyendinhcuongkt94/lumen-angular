<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Category;
class NewsResource extends JsonResource
{
    public function toArray($request)
    {
        return
        [
            'news' => parent::toArray($request),
            'category' => $this->category->name,
            'author' => $this->user->full,
        ];
    }
}
