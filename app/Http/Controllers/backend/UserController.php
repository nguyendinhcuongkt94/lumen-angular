<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use App\User;
use App\Transformers\UserTransformer;
use Illuminate\Support\Collection;
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserTransformer $userTransformer)
    {
        // $this->fractal = $fractal;
        $this->userTransformer = $userTransformer;
    }
    public function index()
    {
        $users = new User;
        return response()->json($users->gets());
    }
    public function detail($id)
    {
        return response()->json(['user'=>User::findorfail($id)]);
    }
    public function save(Request $request)
    {
        
    }
    public function update(Request $req)
    {
        
    }
    public function gets()
    {
        $users = User::all(); // Get users from DB
        $data = new UserTransformer;
        return $data; // Get transformed array of data
    }

}