<?php

namespace App\Http\Controllers\backend;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {

    }

    public function index()
    {
        $categories = new Category;
        return response()->json($categories->gets());
    }

    public function detail($id)
    {
        $category = new Category;
        return response()->json($category->detail($id));
    }

    public function store(Request $request)
    {
        $category = new Category;
        $data = $category->add($request);
        return response()->json($data);
    }

    public function update(Request $request,$id)
    {
        $category = new Category;
        $data = $category->edit($request,$id);
        return response()->json($data);
    }
    
    public function remove($id)
    {
        $category = new Category;
        $category->remove($id);
        return response()->json('Success to remove category');
    }

}
