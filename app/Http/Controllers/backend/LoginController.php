<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login(request $request)
    {

        // header("Access-Control-Allow-Origin: *");
        // header("Access-Control-Request-Headers: GET,POST,OPTIONS,DELETE,PUT");
        // header('Access-Control-Allow-Headers: Accept,Accept-Language,Content-Language,Content-Type');
        $email = $request->get('email');
        $password = $request->get('password');
        $user = User::where('email', $email)->first();
        if ($user) {
            if (Hash::check($password, $user->password)) {
                $apiToken = base64_encode(str_random(40));
                $response = array(
                    'id' => $user->id,
                    'email' => $user->email,
                    'full' => $user->full,
                    'address' => $user->address,
                    'token' => $apiToken,
                    'phone' => $user->phone,
                    'level' => $user->level,
                );
                $user->remember_token = $apiToken;
                $user->save();
            } else {
                $response = array(
                    'error' => 'Email or password is wrong !',
                );
            }
        } else {
            $response = array(
                'error' => 'Email or password is wrong !',
            );
        }
        return response()->json($response);
    }

    public function changePassword(request $request, $id)
    {
        // header("Access-Control-Allow-Origin: *");
        // header("Access-Control-Request-Headers: GET,POST,OPTIONS,DELETE,PUT");
        // header('Access-Control-Allow-Headers: Accept,Accept-Language,Content-Language,Content-Type');
        $user = User::find($id);
        $newpassword = $request->get('newpassword');
        $oldpassword = $request->get('password');
        if (Hash::check($oldpassword, $user->password)) {
            $user->password = Hash::make($newpassword);
            $user->save();
            return response()->json(['success' => 'Success to changed password !']);
        } else {
            return response()->json(['error' => 'False to changed password !']);
        }

    }
    public function test(){
       
    }
}
