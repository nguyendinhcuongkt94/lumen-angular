<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\NewsResource;
use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
    public function index()
    {
        $news = News::orderby('id', 'desc')->get();
        $result = [];
        foreach ($news as $new) {
            $result[] = new NewsResource($new);
        }
        return response()->json($result);
    }
    public function detail($id)
    {
        $data = new NewsResource(News::findorfail($id));
        return response()->json($data);
    }
    public function store(Request $request)
    {
        
        News::create($request->all());
        return response()->json("Success to create new!");
    }

    public function update(Request $request, $id)
    {
        // header("Access-Control-Allow-Origin: *");
        // header("Access-Control-Request-Headers: GET,POST,OPTIONS,DELETE,PUT");
        // header('Access-Control-Allow-Headers: Accept,Accept-Language,Content-Language,Content-Type');
        $new = News::find($id);
        $data = json_decode(json_encode($request->all()),FALSE);
        $new->title = $data->news->title;
        $new->content = $data->news->content;
        $new->img = $data->news->img;
        $new->category_id = $data->news->category_id;
        $new->save();
        return response()->json('Success!');
    }

    public function remove($id)
    {
        News::destroy($id);
        return response()->json('Success delete!');
    }

    public function refineNewsWithCategoryId($id)
    {
        $news = News::where('category_id', 'like', $id)->orderby('id', 'desc')->get();
        $result = [];
        foreach ($news as $new) {
            $result[] = new NewsResource($new);
        }
        return response()->json($result);
    }

    public function search(request $request)
    {
        $key=$request->key;
        $news = News::orwhere('title', 'like', '%' . $key . '%')->orwhere('content', 'like', '%' . $key . '%')->orderby('id','desc')->get();
        $result = [];
        foreach ($news as $new) {
            $result[] = new NewsResource($new);
        }
        return response()->json($result);
    }

}
