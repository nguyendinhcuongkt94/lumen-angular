<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Support\Facades\Hash;


class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'full', 'address', 'phone', 'level',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
    protected $table = 'user';
    public $timestamps = false;

    public function gets()
    {
        $data = $this->all();
        return $data;
    }

    public function detail($id)
    {
        $data = $this->findorfail($id);
        return $data;
    }

    public function add($request)
    {
        $data = $this->create($request->all());
        return $data;
    }

    public function edit($request)
    {
        $data = $this->update($request->all());
        return $data;
    }

    public function remove($id)
    {
        $data = $this->destroy($id);
        return $data;
    }

    public function login($username, $password) 
    {
        $user = User::where('email', $username)->first();
        if(Hash::check($password, $email->password)) {
           $apiToken=base64_encode(str_random(40));
        }
        dd($apiToken);
    }

}
