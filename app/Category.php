<?php

namespace App;

use App\Http\Resources\CategoryResource;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Category extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $table = 'category';
    protected $fillable = [
        'name',
    ];

    public function gets()
    {
        $data = new CategoryResource($this->all());
        return $data;
    }

    public function detail($id)
    {
        $data = $this->findorfail($id);
        return $data;
    }

    public function add($request)
    {
      $data = $this->create($request->all());
      return $data;
    }

    public function edit($request,$id)
    {
       $data = $this->find($id)->update($request->all());
       return $data;
    }

    public function remove($id)
    {
       $data = $this->destroy($id);
       return $data;
    }
}
