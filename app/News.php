<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class News extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $table = 'news';
     protected $fillable = ['title','img','content','category_id','user_id'];
     protected $hidden = ['create_at','updated_at'];

     public function user()
     {
         return $this->belongsTo('App\User', 'user_id', 'id');
     }

     public function category()
     {
         return $this->belongsTo('App\Category', 'category_id', 'id');
     }

}